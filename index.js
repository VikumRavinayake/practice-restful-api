const express = require("express"); // import Express
const Joi = require("joi");
const joi = require("joi"); // import Joi
const app = express(); // create application in app variable
app.use(express.json()); // use json file as db

const customers = [
    {
        name : 'Sherlock',
        id : 1
    },
    {
        name : 'John',
        id : 2
    },
    {
        name : 'Mary',
        id : 3
    },
    {
        name : 'Mycroft',
        id : 4
    }
];

app.get("/",(req,res) => {
    res.send('Welcome to our first RestFul API!');
});

app.get("/api/customers",(req,res) => {
    res.send(customers);
});

app.get("/api/customers/:id",(req,res) => {
    const customer = customers.find((c) => c.id === parseInt(req.params.id));

    if(!customer){
        res.status(404).send(`<h1>Sorry entered customer ID ${req.params.id} dose not exist</h1>`);
    }
    else{
        res.send(customer);
    }

});

app.post("/api/customers",(req,res) => {
    const {error} = validateCustomer(req.body)

    if (error) {
        res.status(404).send(error.details[0].message);
        return
    }

    const newCustomer = {
        name : req.body.name,
        id : customers.length + 1
    }

    customers.push(newCustomer);
    res.send(customers);
}); 

app.put("/api/customers/:id",(req,res) => {
    const customer = customers.find((c) => c.id === parseInt(req.params.id)); // filter customer id

    if (!customer) {
        res.status(404).send(`<h1> Customer you entered dose not exist in database </h1>`); //id condition
    }
   
    // validation part
    const {error} = validateCustomer(req.body);

    if (error) {
        res.status(404).send(error.details[0].message);
    }
    // new update
    customer.name = req.body.name;

    res.send(customers);
});

app.delete("/api/customers/:id",(req,res) =>{
    const customer = customers.find((c) => c.id === parseInt(req.params.id));

    if (!customer) {
        res.status(404).send(`<h1> Customer you entered dose not exist in database </h1>`);
    }

    const index = customers.indexOf(customer);
    customers.splice(index,1);

    res.send(`<h1>Customer related to the customer id ${req.params.id} and customer name ${customer.name} is removed successfully</h1>`);
});

function validateCustomer(customer) {
    const schema = Joi.object({name: Joi.string().min(3).max(10).required()});

    const validation = schema.validate(customer);

    return validation;
}



const port  = process.env.PORT || 3000
app.listen(port, () => console.log(`listening to port ${port}`));
